package com.istic.mmm.arcampus.shared.PI;

import android.app.Activity;

import java.util.HashMap;

/**
 * @deprecated Test class only. Use the factory on production.
 * 
 */
@Deprecated
public class MockPointsOfInterest implements IPointsOfInterest {

    final HashMap<String, GeoPointProxy> gp;

    public MockPointsOfInterest() {
	this.gp = new HashMap<String, GeoPointProxy>();
    }

    @Override
    public HashMap<String, GeoPointProxy> getPointsOfInterest(final Activity act) {

	this.gp.put("Cafétéria Istic", new GeoPointProxy(
		(int) (48.115675 * 1E6), (int) (-1.638138 * 1E6)));
	this.gp.put("Cafétéria Astrolabe", new GeoPointProxy(
		(int) (48.118255 * 1E6), (int) (-1.645203 * 1E6)));
	this.gp.put("R.U. Étoile", new GeoPointProxy((int) (48.122738 * 1E6),
		(int) (-1.640059 * 1E6)));
	this.gp.put("R.U. Astrolabe", new GeoPointProxy((int) (48.11859 * 1E6),
		(int) (-1.635603 * 1E6)));
	this.gp.put("02B (Bato)", new GeoPointProxy((int) (48.116088 * 1E6),
		(int) (-1.638058 * 1E6)));
	this.gp.put("12B", new GeoPointProxy((int) (48.115567 * 1E6),
		(int) (-1.638648 * 1E6)));
	this.gp.put("Administration", new GeoPointProxy(
		(int) (48.115612 * 1E6), (int) (-1.637109 * 1E6)));
	this.gp.put("B.U.", new GeoPointProxy((int) (48.119098 * 1E6),
		(int) (-1.636177 * 1E6)));
	this.gp.put("Scelva", new GeoPointProxy((int) (48.119404 * 1E6),
		(int) (-1.640869 * 1E6)));

	return this.gp;

    }

    @Override
    public String[] getPointsOfInterestAsStringArray(final Activity act) {
	// TODO Auto-generated method stub
	return null;
    }

}
