package com.istic.mmm.arcampus.shared.PI;

import com.google.android.maps.GeoPoint;

import java.io.Serializable;

public class GeoPointProxy extends GeoPoint implements Serializable {

	private static final long serialVersionUID = 1570616242426392339L;
	protected String icon, comment, name;

	public GeoPointProxy(final int latitudeE6, final int longitudeE6) {
		super(latitudeE6, longitudeE6);
		this.icon = "default";
		this.comment = "";
		this.name = "";
	}

	public GeoPointProxy(final int latitudeE6, final int longitudeE6,
			final String name, final String icon, final String comment) {
		super(latitudeE6, longitudeE6);
		this.icon = icon;
		this.comment = comment;
		this.name = name;
	}

	public String getComment() {
		return this.comment;
	}

	public String getIcon() {
		return this.icon;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

}
