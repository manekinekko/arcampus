package com.istic.mmm.arcampus.shared.PI;

import android.app.Activity;
import android.os.Environment;
import android.util.Log;

import com.istic.mmm.arcampus.ApplicationConfig;
import com.istic.mmm.arcampus.R;
import com.istic.mmm.arcampus.shared.utils.JsonPullParser;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class PointsOfInterest implements IPointsOfInterest {

    public static GeoPointProxy getGeoPointByLongName(final String myLongName,
	    final HashMap<String, GeoPointProxy> hm) {

	Log.d("PointsOfInterest", "Finding '" + myLongName
		+ "' in the hasmap of PIs");

	GeoPointProxy geo = null;
	for (final String shortName : hm.keySet()) {
	    geo = hm.get(shortName);

	    Log.d("PointsOfInterest", "Trying to match '" + myLongName
		    + "' with '" + geo.getName() + "' in the hasmap of PIs");

	    if (geo.getName().equals(myLongName)) {

		Log.d("PointsOfInterest", "'" + myLongName
			+ "' was found in the hashmap of PIs");

		return geo;
	    }
	}

	Log.d("PointsOfInterest", "'" + myLongName
		+ "' was NOT found in the hasmap of PIs. Returning NULL.");
	return null;
    }

    public static GeoPointProxy getGeoPointByShortName(
	    final String myShortName, final HashMap<String, GeoPointProxy> hm) {
	for (final String shortName : hm.keySet()) {
	    if (shortName.equals(myShortName)) {
		return hm.get(shortName);
	    }
	}
	return null;
    }

    public static String[] hashMapToArray(
	    final HashMap<String, GeoPointProxy> hm) {
	final String[] s = new String[hm.size()];
	final Set<String> k = hm.keySet();
	final Iterator<String> it = k.iterator();
	int i = 0;
	while (it.hasNext()) {
	    final String element = it.next();
	    s[i++] = element;
	}
	return s;
    }

    @Override
    public HashMap<String, GeoPointProxy> getPointsOfInterest(final Activity act) {
	return JsonPullParser.getPointsOfInterest(Environment
		.getExternalStorageDirectory()
		+ File.separator
		+ act.getString(R.string.app_name)
		+ File.separator
		+ ApplicationConfig.PI_FILENAME);
    }

    @Override
    public String[] getPointsOfInterestAsStringArray(final Activity act) {
	final HashMap<String, GeoPointProxy> hm = JsonPullParser
		.getPointsOfInterest(Environment.getExternalStorageDirectory()
			+ File.separator + act.getString(R.string.app_name)
			+ File.separator + ApplicationConfig.PI_FILENAME);

	final String[] s = new String[hm.size()];
	final Set<String> k = hm.keySet();
	final Iterator<String> it = k.iterator();
	int i = 0;
	while (it.hasNext()) {
	    final String element = it.next();
	    s[i++] = element;
	}
	return s;
    }

}
