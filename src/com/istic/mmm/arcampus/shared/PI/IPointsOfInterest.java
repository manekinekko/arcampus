package com.istic.mmm.arcampus.shared.PI;

import android.app.Activity;

import java.util.HashMap;

public interface IPointsOfInterest {
	HashMap<String, GeoPointProxy> getPointsOfInterest(Activity act);

	String[] getPointsOfInterestAsStringArray(Activity act);
}
