package com.istic.mmm.arcampus.shared.alarm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.istic.mmm.arcampus.R;
import com.istic.mmm.arcampus.ScreenHome;

public class Notifier {

	public static int NOTIFICATION_ID = 1337;

	/**
	 * Send a notification.
	 * 
	 * @param context
	 * @param tickerText
	 *            the message shown when the notification appears
	 * @param title
	 *            the title of the notification
	 * @param message
	 *            the content of the notification
	 */
	public static void sendNotification(Context context, String tickerText,
			String title, String message) {
		NotificationManager mgr = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		Notification note = new Notification(R.drawable.logo_arcampus,
				tickerText, System.currentTimeMillis());
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				new Intent(context, ScreenHome.class), 0);

		note.flags = Notification.FLAG_AUTO_CANCEL;
		note.setLatestEventInfo(context, title, message, intent);

		mgr.notify(NOTIFICATION_ID, note);

	}

	/**
	 * Clear the notification.
	 * 
	 * @param context
	 */
	public static void clearNotification(Context context) {
		NotificationManager mgr = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		mgr.cancel(NOTIFICATION_ID);
	}

}
