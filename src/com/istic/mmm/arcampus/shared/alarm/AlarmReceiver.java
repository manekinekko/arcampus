package com.istic.mmm.arcampus.shared.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.istic.mmm.arcampus.shared.utils.JsonUpdater;

public class AlarmReceiver extends BroadcastReceiver {

	/**
	 * Start the JsonUpdater Service at every AlarmManager event
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			Log.i("alarm", "Received event received");

			// starting the service to update the pi.json
			context.startService(new Intent(context, JsonUpdater.class));

			// receive the extra message:
			// Bundle bundle = intent.getExtras();
			// String message = bundle.getString("alarm_message");
		} catch (Exception e) {
			Log.e("alarm", "Alarm received with errors");
			e.printStackTrace();
		}
	}
}