package com.istic.mmm.arcampus.shared.utils;

import android.location.Location;

/**
 * Temporary handy class to easily get the "best" location of the user.
 * 
 * @author {@link <a href="cheghamwassim.com>Wassim Chegham<a> }
 * @deprecated Not enough time to use this class in the current app.
 * 
 */
@Deprecated
public class LocationUtils {

    private static final int TWO_MINUTES = 1000 * 60 * 2;

    /**
     * Determines whether one Location reading is better than the current
     * Location fix
     * 
     * @param location
     *            The new Location that you want to evaluate
     * @param currentBestLocation
     *            The current Location fix, to which you want to compare the new
     *            one
     */
    protected boolean isBetterLocation(final Location location,
	    final Location currentBestLocation) {
	if (currentBestLocation == null) {
	    // A new location is always better than no location
	    return true;
	}

	// Check whether the new location fix is newer or older
	final long timeDelta = location.getTime()
		- currentBestLocation.getTime();
	final boolean isSignificantlyNewer = timeDelta > LocationUtils.TWO_MINUTES;
	final boolean isSignificantlyOlder = timeDelta < -LocationUtils.TWO_MINUTES;
	final boolean isNewer = timeDelta > 0;

	// If it's been more than two minutes since the current location, use
	// the new location
	// because the user has likely moved
	if (isSignificantlyNewer) {
	    return true;
	    // If the new location is more than two minutes older, it must be
	    // worse
	} else if (isSignificantlyOlder) {
	    return false;
	}

	// Check whether the new location fix is more or less accurate
	final int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation
		.getAccuracy());
	final boolean isLessAccurate = accuracyDelta > 0;
	final boolean isMoreAccurate = accuracyDelta < 0;
	final boolean isSignificantlyLessAccurate = accuracyDelta > 200;

	// Check if the old and new location are from the same provider
	final boolean isFromSameProvider = this.isSameProvider(
		location.getProvider(), currentBestLocation.getProvider());

	// Determine location quality using a combination of timeliness and
	// accuracy
	if (isMoreAccurate) {
	    return true;
	} else if (isNewer && !isLessAccurate) {
	    return true;
	} else if (isNewer && !isSignificantlyLessAccurate
		&& isFromSameProvider) {
	    return true;
	}
	return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(final String provider1,
	    final String provider2) {
	if (provider1 == null) {
	    return provider2 == null;
	}
	return provider1.equals(provider2);
    }

}
