package com.istic.mmm.arcampus.shared.utils.driving;

import com.google.android.maps.GeoPoint;

/**
 * Represents a major placemark along a driving route.
 * 
 */
public interface Placemark {
	public abstract String getDistance();

	public abstract String getInstructions();

	public abstract GeoPoint getLocation();

}