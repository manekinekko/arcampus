package com.istic.mmm.arcampus.shared.utils.driving.impl;

import com.google.android.maps.GeoPoint;
import com.istic.mmm.arcampus.shared.utils.driving.Placemark;
import com.istic.mmm.arcampus.shared.utils.driving.Route;

import java.util.ArrayList;
import java.util.List;

public class RouteImpl implements Route {
	private String totalDistance;
	private List<GeoPoint> geoPoints;
	private List<Placemark> placemarks;

	public void addGeoPoint(final GeoPoint point) {
		if (this.geoPoints == null) {
			this.geoPoints = new ArrayList<GeoPoint>();
		}
		this.geoPoints.add(point);
	}

	public void addPlacemark(final Placemark mark) {
		if (this.placemarks == null) {
			this.placemarks = new ArrayList<Placemark>();
		}
		this.placemarks.add(mark);
	}

	@Override
	public List<GeoPoint> getGeoPoints() {
		return this.geoPoints;
	}

	@Override
	public List<Placemark> getPlacemarks() {
		return this.placemarks;
	}

	@Override
	public String getTotalDistance() {
		return this.totalDistance;
	}

	public void setGeoPoints(final List<GeoPoint> geoPoints) {
		this.geoPoints = geoPoints;
	}

	public void setPlacemarks(final List<Placemark> placemarks) {
		this.placemarks = placemarks;
	}

	public void setTotalDistance(final String totalDistance) {
		this.totalDistance = totalDistance;
	}
}
