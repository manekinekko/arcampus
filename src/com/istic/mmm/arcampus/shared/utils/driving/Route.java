package com.istic.mmm.arcampus.shared.utils.driving;

import com.google.android.maps.GeoPoint;

import java.util.List;

/**
 * Represents the driving directions path. The route consists of a ordered list
 * of geographical points and major placemarks.
 * 
 */
public interface Route {
	public abstract List<GeoPoint> getGeoPoints();

	public abstract List<Placemark> getPlacemarks();

	public abstract String getTotalDistance();

}