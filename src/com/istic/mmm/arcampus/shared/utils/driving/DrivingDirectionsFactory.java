package com.istic.mmm.arcampus.shared.utils.driving;

import com.istic.mmm.arcampus.shared.utils.driving.impl.DrivingDirectionsGoogleKML;

public class DrivingDirectionsFactory {
	public static DrivingDirections createDrivingDirections() {
		return new DrivingDirectionsGoogleKML();
	}
}
