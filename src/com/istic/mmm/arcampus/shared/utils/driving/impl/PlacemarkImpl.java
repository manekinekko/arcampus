package com.istic.mmm.arcampus.shared.utils.driving.impl;

import com.google.android.maps.GeoPoint;
import com.istic.mmm.arcampus.shared.utils.driving.Placemark;

public class PlacemarkImpl implements Placemark {
	private GeoPoint location;
	private String instructions;
	private String distance;

	@Override
	public String getDistance() {
		return this.distance;
	}

	@Override
	public String getInstructions() {
		return this.instructions;
	}

	@Override
	public GeoPoint getLocation() {
		return this.location;
	}

	public void setDistance(final String distance) {
		this.distance = distance;
	}

	public void setInstructions(final String instructions) {
		this.instructions = instructions;
	}

	public void setLocation(final GeoPoint location) {
		this.location = location;
	}
}
