package com.istic.mmm.arcampus.shared.utils;

import android.util.Log;

import com.istic.mmm.arcampus.ApplicationConfig;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

public class FileUtils {

	/**
	 * Read a file and put its content in a String.
	 * 
	 * @param filepath
	 *            the file to read.
	 * @return the file content in a String.
	 * @throws IOException
	 */
	public static String fileToString(String filepath) throws IOException {
		final File json = new File(filepath);
		final BufferedReader br = new BufferedReader(new FileReader(json));
		String file = "";

		String read = br.readLine();
		while (read != null) {
			file += read;
			read = br.readLine();
		}
		br.close();

		return file;

	}

	/**
	 * Write a String into a file. The file is overwrited.
	 * 
	 * @param filepath
	 *            the file to write into
	 * @param content
	 *            the new content of the file
	 * @throws IOException
	 */
	public static void stringToFile(String filepath, String content)
			throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(filepath));
		out.write(content);
		out.close();
	}

	/**
	 * Download a file and put its content in a String.
	 * 
	 * @param url
	 *            the url of the file
	 * @return the file content
	 */
	public static String onlineFileToString(final String url) {

		String dataAsString = null;

		// Create client and set our specific user-agent string
		final HttpClient client = new DefaultHttpClient();
		final HttpGet request = new HttpGet(url);
		request.setHeader("User-Agent", ApplicationConfig.USER_AGENT);

		try {
			final HttpResponse response = client.execute(request);

			// Check if server response is valid
			final StatusLine status = response.getStatusLine();
			if (status.getStatusCode() != 200) {
				throw new IOException("Invalid response from server: "
						+ status.toString());
			}

			// Pull content stream from response
			final HttpEntity entity = response.getEntity();
			final InputStream inputStream = entity.getContent();

			final ByteArrayOutputStream content = new ByteArrayOutputStream();

			// Read response into a buffered stream
			int readBytes = 0;
			final byte[] sBuffer = new byte[512];
			while ((readBytes = inputStream.read(sBuffer)) != -1) {
				content.write(sBuffer, 0, readBytes);
			}

			// Return result from buffered stream
			dataAsString = new String(content.toByteArray());

		} catch (final IOException e) {
			Log.e("FileUtils", e.getLocalizedMessage());
		}
		return dataAsString.trim();

	}

}
