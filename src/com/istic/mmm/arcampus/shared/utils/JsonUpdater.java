package com.istic.mmm.arcampus.shared.utils;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.istic.mmm.arcampus.ApplicationConfig;
import com.istic.mmm.arcampus.R;
import com.istic.mmm.arcampus.shared.alarm.Notifier;

import java.io.File;
import java.io.IOException;

public class JsonUpdater extends Service {

    public class LocalBinder extends Binder {
	JsonUpdater getService() {
	    return JsonUpdater.this;
	}
    }

    private final IBinder mBinder = new LocalBinder();

    @Override
    public IBinder onBind(final Intent arg0) {
	return this.mBinder;
    }

    @Override
    public void onCreate() {
	super.onCreate();
    }

    /**
     * Update the local Json. Download the online version.txt file to compare
     * the version number. Then, if the version is bigger, download the online
     * pi.json to replace the old one. Display a Toast if the service was
     * manually called, else, a notification, to inform the user of the state of
     * the update.
     */
    @Override
    public int onStartCommand(final Intent intent, final int flags,
	    final int startId) {

	Log.i("JsonUpdater", "Starting JsonUpdater");
	final int ret = super.onStartCommand(intent, flags, startId);

	// retrieves extra with the "caller" key, if extra was set
	final String caller = intent.getExtras() != null ? (String) intent
		.getExtras().get("caller") : "";

	final String piPath = Environment.getExternalStorageDirectory()
		+ File.separator + this.getString(R.string.app_name)
		+ File.separator + ApplicationConfig.PI_FILENAME;

	// do not try to download the file if there is no Internet connection
	if (!this.getConnectionState()) {
	    Log.i("JsonUpdater", "Internet connection unavailable! :(");
	    return ret;
	} else {
	    Log.i("JsonUpdater", "Internet connection available! :)");
	}

	final String versionServerString = FileUtils
		.onlineFileToString(ApplicationConfig.URL_VERSION);

	try {
	    final float versionServer = Float.parseFloat(versionServerString);
	    final float versionLocal = JsonPullParser.getVersion(piPath);
	    Notifier.clearNotification(this.getApplicationContext());

	    // the version does not correspond to a date anymore. Instead, we
	    // use a number
	    if (versionServer > versionLocal) {
		try {
		    final String piServer = FileUtils
			    .onlineFileToString(ApplicationConfig.URL_PI);
		    FileUtils.stringToFile(piPath, piServer);

		    FileUtils.stringToFile(piPath, piServer);
		    Log.i("JsonUpdater", "Json file updated");

		    if (caller.equals("manual")) {
			Toast.makeText(
				this.getApplicationContext(),
				"The database has been updated to version "
					+ versionServer + "!",
				Toast.LENGTH_LONG).show();
		    } else {
			Notifier.sendNotification(this.getApplicationContext(),
				"New database update", "ARCampus updated",
				"The ARCampus database has been updated to version "
					+ versionServer + "!");
		    }
		} catch (final IOException e) {
		    e.printStackTrace();
		    Log.e("JsonUpdater", "IOError while updating the json file");
		}
	    } else {
		Log.i("JsonUpdater", "The json file is already up to date");

		if (caller.equals("manual")) {
		    Toast.makeText(
			    this.getApplicationContext(),
			    "The application's database is already up to date!",
			    Toast.LENGTH_LONG).show();
		}
	    }

	} catch (final NumberFormatException e) {
	    Log.e("JsonUpdater",
		    "Cannot parse versionServer: " + e.getLocalizedMessage());
	} catch (final NullPointerException e) {
	    Log.e("JsonUpdater",
		    "Cannot parse versionServer: " + e.getLocalizedMessage());
	} finally {
	    this.stopSelf();
	}
	return ret;
    }

    /**
     * 
     * @return true if the device is connected on the Internet
     */
    private boolean getConnectionState() {
	final ConnectivityManager conMgr = (ConnectivityManager) this
		.getApplicationContext().getSystemService(
			Context.CONNECTIVITY_SERVICE);

	final NetworkInfo i = conMgr.getActiveNetworkInfo();
	if (i == null) {
	    return false;
	}
	if (!i.isConnected()) {
	    return false;
	}
	if (!i.isAvailable()) {
	    return false;
	}
	return true;
    }
}
