package com.istic.mmm.arcampus.shared.utils;

import android.util.Log;

import com.istic.mmm.arcampus.ApplicationConfig;
import com.istic.mmm.arcampus.shared.PI.GeoPointProxy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

public class JsonPullParser {

	/**
	 * Parse the pi.json and retrieve all its points of interest. It creates a
	 * HashMap, where the the key is the pi's "shortname", and the value, a
	 * GeoPointProxy.
	 * 
	 * @param filepath
	 *            the path of the pi.json file.
	 * @return
	 */
	public static HashMap<String, GeoPointProxy> getPointsOfInterest(
			final String filepath) {

		String content = null;
		try {
			content = FileUtils.fileToString(filepath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return getPointsOfInterestFromString(content);
	}

	public static HashMap<String, GeoPointProxy> getPointsOfInterestFromString(
			final String content) {

		final HashMap<String, GeoPointProxy> pointOfInterest = new HashMap<String, GeoPointProxy>();

		try {

			final JSONObject jObject = new JSONObject(content);
			final JSONArray PIArray = jObject.getJSONArray("PI");

			float lat, lng;
			GeoPointProxy point;
			String pointShortName, pointName, comment, icon;

			for (int j = 0; j < PIArray.length(); j++) {
				pointShortName = PIArray.getJSONObject(j)
						.getString("shortname").toString();
				pointName = PIArray.getJSONObject(j).getString("name")
						.toString();
				comment = PIArray.getJSONObject(j).getString("comment")
						.toString();
				icon = PIArray.getJSONObject(j).getString("icon").toString();

				lat = Float.parseFloat((PIArray.getJSONObject(j).getString(
						"lat").toString()));
				lng = Float.parseFloat((PIArray.getJSONObject(j).getString(
						"lng").toString()));
				point = new GeoPointProxy((int) (lat * 1E6), (int) (lng * 1E6),
						pointName, icon, comment);

				pointOfInterest.put(pointShortName, point);

			}

		} catch (final JSONException e) {
			Log.e("JsonParser", e.getLocalizedMessage());
			Log.e("JsonParser", ApplicationConfig.PI_FILENAME
					+ " could not be parsed!");
		}

		return pointOfInterest;

	}

	/**
	 * Get a version number from a json file
	 * 
	 * @param filepath
	 *            the file containing the version
	 * @return the version number
	 */
	public static float getVersion(final String filepath) {

		try {
			return JsonPullParser.getVersionFromString(FileUtils
					.fileToString(filepath));
		} catch (final IOException e) {
			e.printStackTrace();
		}

		return -1;

	}

	/**
	 * Return a version from a String
	 * 
	 * @param content
	 *            the string containing the version number. Must a be in a Json
	 *            format
	 * @return the version number
	 */
	public static float getVersionFromString(final String content) {

		JSONObject jObject;
		try {
			jObject = new JSONObject(content);
			return (float) jObject.getDouble("version");
		} catch (final JSONException e) {
			e.printStackTrace();
		}

		return -1;
	}

}
