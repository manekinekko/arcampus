package com.istic.mmm.arcampus.menus;

public final class OptionsMenuFactory {

	public static IOptionsMenu createModeMapOptionsMenu() {
		return ModeMapOptionsMenu.getInstance();
	}
	
	public static IOptionsMenu createModeAROptionsMenu() {
		return ModeMapOptionsMenu.getInstance();
	}
}
