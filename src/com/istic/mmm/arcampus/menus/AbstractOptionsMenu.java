package com.istic.mmm.arcampus.menus;

public abstract class AbstractOptionsMenu {

	protected static IOptionsMenu instance;
	
	private AbstractOptionsMenu() {}
	
	abstract IOptionsMenu getInstance();
	abstract boolean handleUserAction(int itemId);
	
}
