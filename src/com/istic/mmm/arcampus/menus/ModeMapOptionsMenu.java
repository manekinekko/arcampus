package com.istic.mmm.arcampus.menus;

import android.content.Context;
import android.content.Intent;

import com.istic.mmm.arcampus.R;
import com.istic.mmm.arcampus.modes.ScreenModeAR;
import com.istic.mmm.arcampus.modes.ScreenModeMap;
import com.istic.mmm.arcampus.modes.ScreenModePI;
import com.istic.mmm.arcampus.modes.ScreenModePrefs;

public class ModeMapOptionsMenu implements IOptionsMenu {

    private static IOptionsMenu instance;

    private ModeMapOptionsMenu() {
    }

    public static IOptionsMenu getInstance() {
	if (ModeMapOptionsMenu.instance == null) {
	    ModeMapOptionsMenu.instance = new ModeMapOptionsMenu();
	}
	return ModeMapOptionsMenu.instance;
    }

    @Override
    public boolean handleUserAction(final int itemId, final Context ctx) {
	switch (itemId) {
	case R.id.mode_map_menu_show_user_position:
	    ScreenModeMap.animateToUserPosition(20);
	    return true;
	case R.id.mode_map_menu_show_gps_accuracy:
	    ((ScreenModeMap) ctx).togglePositionAccuracyArea(ctx);
	    return true;

	case R.id.mode_map_menu_switch_to_mode_ar:
	    ctx.startActivity(new Intent(ctx, ScreenModeAR.class));
	    return true;

	case R.id.mode_menu_pi_list:
	    ctx.startActivity(new Intent(ctx, ScreenModePI.class));
	    return true;

	case R.id.mode_map_menu_switch_to_mode_map:
	    ctx.startActivity(new Intent(ctx, ScreenModeMap.class));
	    return true;

	case R.id.mode_prefs:
	    ctx.startActivity(new Intent(ctx, ScreenModePrefs.class));
	    return true;

	default:
	    return false;
	}
    }
}