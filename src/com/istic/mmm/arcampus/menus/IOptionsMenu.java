package com.istic.mmm.arcampus.menus;


import android.content.Context;

public interface IOptionsMenu {
    public boolean handleUserAction(int itemId, Context ctx);
}
