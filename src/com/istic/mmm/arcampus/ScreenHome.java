package com.istic.mmm.arcampus;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.istic.mmm.arcampus.listener.BtnOnClickListenerStartActivity;
import com.istic.mmm.arcampus.listener.BtnOnClickListenerStartSettings;
import com.istic.mmm.arcampus.menus.OptionsMenuFactory;
import com.istic.mmm.arcampus.modes.ScreenModeAR;
import com.istic.mmm.arcampus.modes.ScreenModeMap;
import com.istic.mmm.arcampus.modes.ScreenModePI;

public class ScreenHome extends Activity implements LocationListener {

    Button btnActivateGPS;
    ImageButton btnModeAR, btnModeMap, btnPIsList;

    @Override
    public void onCreate(final Bundle savedInstanceState) {

	super.onCreate(savedInstanceState);
	this.setContentView(R.layout.screen_home);

	this.btnModeAR = (ImageButton) this.findViewById(R.id.btn_mode_ar);
	this.btnModeMap = (ImageButton) this.findViewById(R.id.btn_mode_map);
	this.btnPIsList = (ImageButton) this.findViewById(R.id.btn_pi_list);
	this.btnActivateGPS = (Button) this.findViewById(R.id.btn_activate_gps);

	this.btnModeAR.setOnClickListener(new BtnOnClickListenerStartActivity(
		this, ScreenModeAR.class));
	this.btnModeMap.setOnClickListener(new BtnOnClickListenerStartActivity(
		this, ScreenModeMap.class));
	this.btnPIsList.setOnClickListener(new BtnOnClickListenerStartActivity(
		this, ScreenModePI.class));
	this.btnActivateGPS
		.setOnClickListener(new BtnOnClickListenerStartSettings(this,
			Settings.ACTION_LOCATION_SOURCE_SETTINGS));

	this.checkLocationSourcesState();

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
	super.onCreateOptionsMenu(menu);
	final MenuInflater inflater = this.getMenuInflater();
	inflater.inflate(R.layout.screen_home_menu, menu);
	return true;
    }

    @Override
    public void onLocationChanged(final Location location) {
    }

    @Override
    /**
     * Delegate the work to the Menu factory.
     */
    public boolean onMenuItemSelected(final int featureId, final MenuItem item) {
	super.onMenuItemSelected(featureId, item);
	final boolean b = OptionsMenuFactory.createModeMapOptionsMenu()
		.handleUserAction(item.getItemId(), this);
	return b ? b : super.onContextItemSelected(item);
    }

    @Override
    public void onProviderDisabled(final String provider) {
	this.btnActivateGPS.setVisibility(View.VISIBLE);
	this.btnActivateGPS.setEnabled(true);
	this.btnActivateGPS.setText(this.getText(R.string.btn_activate_gps));
    }

    @Override
    public void onProviderEnabled(final String provider) {
	this.btnActivateGPS.setVisibility(View.INVISIBLE);
	this.btnActivateGPS.setEnabled(false);
	this.btnActivateGPS.setText(this.getText(R.string.btn_activate_gps_ok));
    }

    @Override
    public void onStatusChanged(final String provider, final int status,
	    final Bundle extras) {
    }

    private void checkLocationSourcesState() {
	final LocationManager locationManager = (LocationManager) this
		.getSystemService(Context.LOCATION_SERVICE);
	locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
		1000L, 1.0f, this);
	final boolean isGPS = locationManager
		.isProviderEnabled(LocationManager.GPS_PROVIDER);
	if (isGPS) {
	    this.btnActivateGPS.setVisibility(View.INVISIBLE);
	    // this.btnActivateGPS.setEnabled(false);
	    // this.btnActivateGPS.setText(this.getText(R.string.btn_activate_gps_ok));
	} else {
	    this.btnActivateGPS.setVisibility(View.VISIBLE);
	    // this.btnActivateGPS.setEnabled(true);
	    // this.btnActivateGPS.setText(this.getText(R.string.btn_activate_gps));
	}
    }

}