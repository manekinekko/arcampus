package com.istic.mmm.arcampus;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import com.istic.mmm.arcampus.shared.alarm.AlarmReceiver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class ApplicationConfig {

    public static final String PI_FILENAME = "pi.json";
    public static final String URL_PI = "http://arcampus.cheghamwassim.com/pi.json";
    public static final String URL_VERSION = "http://arcampus.cheghamwassim.com/version.txt";
    public static final String USER_AGENT = "Mozilla/5.0 (Linux; U; Android 2.2; en-us; Nexus One Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1";
    public static final int ALARM_ID = 98724;
    public static final long UPDATE_INTERVAL = AlarmManager.INTERVAL_DAY;

    public static int getPIDrawable(final String icon) {

	if (icon.equals("building")) {
	    return R.drawable.icon_map_red;
	} else if (icon.equals("resto")) {
	    return R.drawable.icon_map_blue;
	} else if (icon.equals("default")) {
	    return R.drawable.icon_map_green;
	}

	return R.drawable.icon_map_red;
    }

    public static boolean isAvailableAppDirectory(final Activity act) {
	return new File(Environment.getExternalStorageDirectory()
		+ File.separator + act.getString(R.string.app_name)) != null;
    }

    public static void prepareAppDirectoryOnSDCard(final Activity act) {
	ApplicationConfig.installAppDirectory(act);
	ApplicationConfig.copyFileToAppDirectory(act);
    }

    public static void runAlarm(final Context context, final Activity activity) {
	final Intent intent = new Intent(context, AlarmReceiver.class);
	// send message with the alarm
	// intent.putExtra("alarm_message", "O'Doyle Rules!");

	final PendingIntent sender = PendingIntent.getBroadcast(context,
		ApplicationConfig.ALARM_ID, intent,
		PendingIntent.FLAG_UPDATE_CURRENT);

	// Get the AlarmManager service
	final AlarmManager am = (AlarmManager) activity
		.getSystemService(Context.ALARM_SERVICE);
	am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
		SystemClock.elapsedRealtime(),
		ApplicationConfig.UPDATE_INTERVAL, sender);
    }

    private static void copyFileToAppDirectory(final Activity act) {

	if (!Environment.MEDIA_MOUNTED.equals(Environment
		.getExternalStorageState())) {
	    Toast.makeText(act, "External SD card not available!",
		    Toast.LENGTH_LONG).show();
	} else {
	    final String outFilePath = Environment
		    .getExternalStorageDirectory()
		    + File.separator
		    + act.getString(R.string.app_name)
		    + File.separator
		    + ApplicationConfig.PI_FILENAME;

	    final File pi = new File(outFilePath);

	    if (!pi.exists()) {
		try {
		    InputStream in = act.getResources().openRawResource(
			    R.raw.pi);
		    OutputStream out = new FileOutputStream(outFilePath);
		    final byte[] buffer = new byte[1024];
		    int read;
		    while ((read = in.read(buffer)) != -1) {
			out.write(buffer, 0, read);
		    }
		    in.close();
		    in = null;
		    out.flush();
		    out.close();
		    out = null;
		} catch (final Exception e) {
		    Log.e("ApplicationConfig", "Could not copy "
			    + ApplicationConfig.PI_FILENAME + " to sdcard!"
			    + "\n" + Log.getStackTraceString(e));
		}
	    }

	}

    }

    private static void installAppDirectory(final Activity act) {
	if (!Environment.getExternalStorageState().equals(
		Environment.MEDIA_MOUNTED)) {
	} else {
	    final File directory = new File(
		    Environment.getExternalStorageDirectory() + File.separator
			    + act.getString(R.string.app_name));
	    directory.mkdirs();
	}
    }

}