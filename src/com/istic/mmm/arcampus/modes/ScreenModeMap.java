package com.istic.mmm.arcampus.modes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.istic.mmm.arcampus.ApplicationConfig;
import com.istic.mmm.arcampus.R;
import com.istic.mmm.arcampus.menus.OptionsMenuFactory;
import com.istic.mmm.arcampus.modes.map.AccuracyAreaOverlay;
import com.istic.mmm.arcampus.modes.map.CompassOverlay2;
import com.istic.mmm.arcampus.modes.map.DirectionPathOverlay;
import com.istic.mmm.arcampus.modes.map.DirectionPlacemarkOverlay;
import com.istic.mmm.arcampus.modes.map.DrivingDirectionCallback;
import com.istic.mmm.arcampus.modes.map.MarkersOverlay;
import com.istic.mmm.arcampus.shared.PI.GeoPointProxy;
import com.istic.mmm.arcampus.shared.PI.PointsOfInterest;
import com.istic.mmm.arcampus.shared.PI.PointsOfInterestFactory;
import com.istic.mmm.arcampus.shared.utils.driving.DrivingDirections.Mode;
import com.istic.mmm.arcampus.shared.utils.driving.DrivingDirectionsFactory;
import com.istic.mmm.arcampus.shared.utils.driving.impl.DrivingDirectionsGoogleKML;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ScreenModeMap extends MapActivity {

    public static final String APP_TAG = "ScreenModeMap";
    private float mGpsUpdateMoveInterval = 0;
    private long mGpsUpdateTimeInterval = 0;
    private CompassOverlay2 mCompass;
    private double mLatitude = 0;
    private double mLongitude = 0;
    private static AccuracyAreaOverlay mAccuracyAreaOverlay = new AccuracyAreaOverlay();
    private static MapView mMapView = null;
    private static GeoPoint mUserCurrentGeoPoint = null;
    private static GeoPoint mCachedUserCurrentGeoPoint = null;
    private static HashMap<String, GeoPointProxy> mPointsOfInterestHashMap = null;
    private static boolean mToggleMyLocation = false;
    private List<Overlay> mMapOverlaysList = null;
    private Location mLocation = null;
    private LocationListener mlocListener = null;
    private LocationManager mlocManager = null;
    private AlertDialog mWaitingDialog = null;
    private SensorManager mSensorManager = null;
    private Sensor mSensor = null;
    private Sensor mAccelerometer = null;
    private Sensor mMagnetometer = null;
    private GeoPointProxy mLastSelectedPoint = null;
    private DirectionPathOverlay mCachedDirections = null;
    private DirectionPlacemarkOverlay mCachedPlacemarkers = null;
    private static ScreenModeMap instance = null;
    private static boolean mIsDirectionPathOn = false;

    /**
     * This static method shows the user current location. It actually makes a
     * few calls to the Google Map controller.
     */
    public static void animateToUserPosition(final int zoomLevel) {
	final MapController mapController = ScreenModeMap.mMapView
		.getController();
	mapController.animateTo(ScreenModeMap.mUserCurrentGeoPoint);
	mapController.setZoom(zoomLevel);
    }

    /**
     * This static method is part of the Singleton Pattern which makes sure to
     * have only one instance throughout all the application session.
     * 
     * @return The instance of the ScreenModeMap.
     */
    public static ScreenModeMap getInstance() {
	if (ScreenModeMap.instance == null) {
	    ScreenModeMap.instance = new ScreenModeMap();
	}
	return ScreenModeMap.instance;
    }

    /**
     * @return The userCurrentGeoPoint
     */
    public static GeoPoint getUserCurrentGeoPoint() {
	return ScreenModeMap.mUserCurrentGeoPoint;
    }

    /**
     * This static method is a setter that update the state of the
     * {@link ScreenModeMap.mDirectionPath} inner property. This property allows
     * the application whether to draw the direction path on the map or clear
     * it.
     * 
     * @param b
     *            - If passing True, the application draws the direction path.
     *            False clears the last drawn path.
     * @see {@link ScreenModeMap.showDirectionToGeoPoint()}
     */
    public static void setDirectionPath(final boolean b) {
	ScreenModeMap.mIsDirectionPathOn = b;
    }

    /**
     * This static method sets the user's current position.
     * 
     * @param userCurrentGeoPoint
     *            - The user's current location (as a GeoPoint). This location
     *            is usually provided by a location provider.
     */
    public static void setUserCurrentGeoPoint(final GeoPoint userCurrentGeoPoint) {
	// cache the current user location
	ScreenModeMap.mCachedUserCurrentGeoPoint = ScreenModeMap.mUserCurrentGeoPoint;

	// set the new user location
	ScreenModeMap.mUserCurrentGeoPoint = userCurrentGeoPoint;
    }

    /**
     * This static method allows the application to know whether the
     * ScreenModeMap should follow the user's location.
     */
    public static void toggleFollowUserLocation() {
	ScreenModeMap.mToggleMyLocation = !ScreenModeMap.mToggleMyLocation;
    }

    /**
     * This clears the previous direction path overlay.
     */
    public void clearPreviousDirectionPath() {

	// this will clear the map overlays !!
	ScreenModeMap.mIsDirectionPathOn = false;
	this.setGeoPostionsOnMapView(ScreenModeMap.mMapView);

	Log.d(ScreenModeMap.APP_TAG, "Clearing previous direction...");

    }

    /**
     * @return The last cached direction overlay.
     */
    public DirectionPathOverlay getCachedDirections() {
	return this.mCachedDirections;
    }

    /**
     * @return The map overlay list items.
     */
    public List<Overlay> getMapOverlaysList() {
	return this.mMapOverlaysList;
    }

    /**
     * @return The map view.
     */
    public MapView getMapView() {
	return ScreenModeMap.mMapView;
    }

    public boolean isDirectionPathOn() {
	return ScreenModeMap.mIsDirectionPathOn == true;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {

	super.onCreate(savedInstanceState);
	this.setContentView(R.layout.mode_map);

	// initiate the points of interest
	ScreenModeMap.mPointsOfInterestHashMap = (new PointsOfInterestFactory())
		.createPointsOfInterest().getPointsOfInterest(this);

	// initiate the map view
	ScreenModeMap.mMapView = (MapView) this.findViewById(R.id.map);
	ScreenModeMap.mMapView.setBuiltInZoomControls(true);
	ScreenModeMap.mMapView.setSatellite(true);
	ScreenModeMap.mMapView.setSaveEnabled(true);

	// sensors
	this.mSensorManager = (SensorManager) this
		.getSystemService(Context.SENSOR_SERVICE);
	this.mCompass = new CompassOverlay2(this);
	this.mSensor = this.mSensorManager
		.getDefaultSensor(Sensor.TYPE_ORIENTATION);
	this.mAccelerometer = this.mSensorManager
		.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	this.mMagnetometer = this.mSensorManager
		.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

	this.initLocation(ScreenModeMap.mMapView);
	if (ScreenModeMap.mUserCurrentGeoPoint == null) {
	}

	// filters and bundles
	final Bundle bundle = this.getIntent().getExtras();
	if (bundle != null) {
	    final String action = this.getIntent().getAction();
	    if (action != null) {
		this.handleIntentFilterAction(action);
	    } else {
		this.handleTransferedBundle(bundle);
	    }
	} else {
	    ScreenModeMap.animateToUserPosition(20);
	}

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
	super.onCreateOptionsMenu(menu);
	final MenuInflater inflater = this.getMenuInflater();
	inflater.inflate(R.layout.mode_map_menu, menu);
	return true;
    }

    @Override
    /**
     * Delegate the work to the Menu factory.
     */
    public boolean onMenuItemSelected(final int featureId, final MenuItem item) {
	super.onMenuItemSelected(featureId, item);
	final boolean b = OptionsMenuFactory.createModeMapOptionsMenu()
		.handleUserAction(item.getItemId(), this);
	return b ? b : super.onContextItemSelected(item);
    }

    public void setCachedDirections(final DirectionPathOverlay mCachedDirections) {
	this.mCachedDirections = mCachedDirections;
    }

    public void setCachedPlacemarks(
	    final DirectionPlacemarkOverlay directionPlacemarkOverlay) {
	this.mCachedPlacemarkers = directionPlacemarkOverlay;
    }

    public void setGeoPostionsOnMapView(final MapView mapView) {

	this.mMapOverlaysList = mapView.getOverlays();
	if (!this.mMapOverlaysList.isEmpty()) {
	    this.mMapOverlaysList.clear();
	    mapView.invalidate();
	}

	// handle the user's marker
	Drawable drawable = this.getResources().getDrawable(
		R.drawable.icon_map_green);
	MarkersOverlay markersOverlay = new MarkersOverlay(drawable, this);

	// handle the user's position
	GeoPointProxy point = new GeoPointProxy((int) (this.mLatitude * 1E6),
		(int) (this.mLongitude * 1E6));
	OverlayItem overlayitem = new OverlayItem(point, "You",
		"This is your current location.");
	markersOverlay.addOverlay(overlayitem);
	this.mMapOverlaysList.add(markersOverlay);
	ScreenModeMap.setUserCurrentGeoPoint(point);

	// handle the PoI positions
	/**
	 * TODO Since the position of the PoI won't change, it would be better
	 * to process the following code only once (in the onCreate method)!
	 * Keeping it in this method is a bad idea because this method gets
	 * called on each gps status update !!
	 */
	ScreenModeMap.mPointsOfInterestHashMap = (new PointsOfInterestFactory())
		.createPointsOfInterest().getPointsOfInterest(this);
	final Iterator<String> pointsOfInterestHashMapIterator = ScreenModeMap.mPointsOfInterestHashMap
		.keySet().iterator();
	String key;
	while (pointsOfInterestHashMapIterator.hasNext()) {
	    key = pointsOfInterestHashMapIterator.next();
	    point = ScreenModeMap.mPointsOfInterestHashMap.get(key);

	    drawable = this.getResources().getDrawable(
		    ApplicationConfig.getPIDrawable(point.getIcon()));
	    markersOverlay = new MarkersOverlay(drawable, this);
	    overlayitem = new OverlayItem(point,
		    ScreenModeMap.mPointsOfInterestHashMap.get(key).getName(),
		    point.getComment());
	    markersOverlay.addOverlay(overlayitem);
	    this.mMapOverlaysList.add(markersOverlay);

	}

	// handle the GPS accuracy
	this.showAccuracyArea();

	// and finally handle the compass
	this.showCompass();

	// should
	if (ScreenModeMap.mIsDirectionPathOn) {
	    this.showDirectionToGeoPoint(
		    MarkersOverlay.getLastSelectedGeoPoint(), true);
	}

    }

    public void setWaitingMessage(final boolean b) {
	if (this.mWaitingDialog != null) {
	    if (b) {
		this.mWaitingDialog = ProgressDialog.show(this, "",
			"Please wait...", true);

		Log.d(ScreenModeMap.APP_TAG, "showing loading dialog...");

	    } else {
		this.mWaitingDialog.dismiss();
		Log.d(ScreenModeMap.APP_TAG, "closing loading dialog...");
	    }
	}
    }

    /**
     * This static method draws a direction from the current user location to a
     * given location. <b>Please note that this method gets the direction
     * information directly from Google Direction server!</b>.
     * 
     * @param geoPoint
     *            - A given location (as a GeoPoint) to where we need to draw a
     *            direction.
     * 
     * @see {@link ScreenModeMap.showCachedDirectionToGeoPoint()}
     */
    public void showDirectionToGeoPoint(final GeoPointProxy geoPoint,
	    final boolean forceServer) {

	final ScreenModeMap screenModeMap = ScreenModeMap.getInstance();

	if (forceServer) {
	    screenModeMap.mCachedDirections = null;
	}

	// force accessing the remote server only if there are no cached
	// directions or if the user's location has changed.
	if ((screenModeMap.mCachedDirections == null)
		|| !ScreenModeMap.mUserCurrentGeoPoint
			.equals(ScreenModeMap.mCachedUserCurrentGeoPoint)) {

	    Log.d(ScreenModeMap.APP_TAG,
		    "Drawing direction path from remote KML...");

	    ScreenModeMap.mIsDirectionPathOn = true;

	    try {

		screenModeMap.mLastSelectedPoint = geoPoint;

		final GeoPoint start = ScreenModeMap.mUserCurrentGeoPoint;

		((DrivingDirectionsGoogleKML) DrivingDirectionsFactory
			.createDrivingDirections()).driveTo(start,
			screenModeMap.mLastSelectedPoint, Mode.WALKING,
			new DrivingDirectionCallback(screenModeMap));

	    } catch (final Exception e) {

		this.showToast("Sorry! Your location was not set correctly. We are unable to get your direction.");
		Log.d(ScreenModeMap.APP_TAG,
			"Sorry! Your location was not set correctly. We are unable to get your direction.");

	    }

	} else {

	    Log.d(ScreenModeMap.APP_TAG,
		    "Drawing direction path from cached KML...");

	    this.showCachedDirectionToGeoPoint(MarkersOverlay
		    .getLastSelectedGeoPoint());

	}

    }

    /**
     * This static method shows a given PI information <b>(this method is not
     * yet implemented!)</b>.
     * 
     * @param selectedGeoPoint
     *            - A PI (as a GeoPoint).
     */
    public void showGeoPointInformation(final GeoPointProxy selectedGeoPoint) {
	this.showToast("Not implemented yet!");
    }

    public void showToast(final String string) {
	Toast.makeText(this, string, Toast.LENGTH_LONG).show();
    }

    /**
     * This static method can be called to draw an approximative area indicating
     * the current provider accuracy.
     * 
     * @param ctx
     *            - The application context.
     */
    public void togglePositionAccuracyArea(final Context ctx) {
	if (((ScreenModeMap) ctx).mLocation != null) {
	    ScreenModeMap.mAccuracyAreaOverlay.toggleDraw();
	} else {
	    this.showToast("Sorry! Your location is not available yet! Please try later.");
	}
    }

    /**
     * Method to assign GPS prefs.
     */
    public void updateGPSprefs() {
	final int gpsPref = Integer.parseInt(ScreenModePrefs.getGPSPref(this
		.getApplicationContext()));
	switch (gpsPref) {
	case 1:
	    this.mGpsUpdateTimeInterval = 5000; // milliseconds
	    this.mGpsUpdateMoveInterval = 1; // meters
	    break;
	case 2:
	    this.mGpsUpdateTimeInterval = 10000;
	    this.mGpsUpdateMoveInterval = 100;
	    break;
	case 3:
	    this.mGpsUpdateTimeInterval = 125000;
	    this.mGpsUpdateMoveInterval = 1000;
	    break;
	}
    }

    @Override
    protected boolean isRouteDisplayed() {
	return false;
    }

    @Override
    protected void onActivityResult(final int requestCode,
	    final int resultCode, final Intent data) {
	super.onActivityResult(requestCode, resultCode, data);

	if (resultCode == Activity.RESULT_CANCELED) {
	    Log.d(ScreenModeMap.APP_TAG, "PI Activity was cancelled - data ="
		    + data.getExtras().getString("selected_pi"));
	}

    }

    @Override
    protected void onDestroy() {
	this.mlocManager.removeUpdates(this.mlocListener);
	// ScreenModeMap.mMapView.saveState();
	super.onDestroy();
    }

    @Override
    protected void onPause() {
	super.onPause();
	this.mlocManager.removeUpdates(this.mlocListener);
	ScreenModeMap.mMapView.invalidate();
	this.mMapOverlaysList.clear();
    }

    @Override
    protected void onResume() {
	super.onResume();
	this.initLocation(ScreenModeMap.mMapView);
	this.mSensorManager.registerListener(this.mCompass.getListener(),
		this.mSensor, SensorManager.SENSOR_DELAY_UI);
	this.mSensorManager.registerListener(this.mCompass.getListener(),
		this.mAccelerometer, SensorManager.SENSOR_DELAY_UI);
	this.mSensorManager.registerListener(this.mCompass.getListener(),
		this.mMagnetometer, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onStop() {
	this.mSensorManager.unregisterListener(this.mCompass.getListener());
	super.onStop();
    }

    /**
     * This method handles the interaction with Google Calendar in order to
     * share the location of a given PI.
     * 
     * @param action
     *            - an Intent.ACTION_VIEW action.
     */
    private void handleIntentFilterAction(final String action) {
	if (Intent.ACTION_VIEW.equals(action)) {
	    if (this.getIntent().getExtras()
		    .getString("com.android.browser.application_id")
		    .equals("com.google.android.calendar")) {

		// parse the given PI name: from B02-E106 to b02
		final String building_info_from_calendar = this.getIntent()
			.getDataString().split("q=")[1].split("-")[0]
			.toLowerCase();
		final Set<String> set = ScreenModeMap.mPointsOfInterestHashMap
			.keySet();

		// look for the corresponding building name in the PI hashmap
		String tmp_pi_name = "";
		for (final String pi_shortname : set) {
		    if (pi_shortname.contains(building_info_from_calendar)) {
			tmp_pi_name = pi_shortname;
			break;
		    }
		}

		//
		if (tmp_pi_name.equals("")) {
		    this.showToast("Sorry! We couldn't find '"
			    + tmp_pi_name.toUpperCase() + "' on the map.");
		} else {
		    final GeoPointProxy geo = PointsOfInterest
			    .getGeoPointByShortName(tmp_pi_name,
				    ScreenModeMap.mPointsOfInterestHashMap);
		    final MapController mapController = ScreenModeMap.mMapView
			    .getController();
		    mapController.animateTo(new GeoPoint(geo.getLatitudeE6(),
			    geo.getLongitudeE6()));
		    mapController.setZoom(25);

		    this.showToast("Finding '" + geo.getName() + "'...");
		}

	    }
	}
    }

    /**
     * This method handles the transfered bundle coming from the PI activity.
     * 
     * @param b
     *            - The bundle containing the required info.
     */
    private void handleTransferedBundle(final Bundle b) {

	/**
	 * This piece of code make only sure to animate to the location of the
	 * selected PI or draw a direction path toward it.
	 */
	final boolean showDirection = b.containsKey("direction_pi");
	final boolean showPI = b.containsKey("selected_pi");

	if (showPI || showDirection) {

	    // get the PI name from either the PI activity or the AR mode.
	    String selected_pi_longName = b.getString("selected_pi");
	    selected_pi_longName = selected_pi_longName == null ? b
		    .getString("direction_pi") : selected_pi_longName;

	    // get its geo point
	    final GeoPointProxy selected_geo = PointsOfInterest
		    .getGeoPointByLongName(selected_pi_longName,
			    ScreenModeMap.mPointsOfInterestHashMap);

	    if (selected_geo != null) {

		this.showToast("Finding '" + selected_pi_longName + "'...");

		Log.d(ScreenModeMap.APP_TAG, selected_pi_longName + " @ "
			+ selected_geo.toString());

		// clear previous path
		this.clearPreviousDirectionPath();

		// should we draw the direction to that PI
		if (showDirection) {
		    Log.d(ScreenModeMap.APP_TAG, "showing direction to "
			    + selected_pi_longName + " @ " + selected_geo);
		    this.showDirectionToGeoPoint(selected_geo, true);
		}
		// or just show it on the map
		else if (showPI) {
		    Log.d(ScreenModeMap.APP_TAG, "zooming on "
			    + selected_pi_longName + " @ " + selected_geo);
		    final MapController mapController = ScreenModeMap.mMapView
			    .getController();
		    mapController.animateTo(selected_geo);
		    mapController.setZoom(20);

		}

	    } else {
		this.showToast("Sorry! We couldn't find '"
			+ selected_pi_longName + "'.");
		Log.e(ScreenModeMap.APP_TAG, "The PI: " + selected_pi_longName
			+ " was not found in pi.json");
	    }

	}
    }

    /**
     * This method does initiate all the required locations. It uses the
     * location manager in order
     * 
     * @param mapView
     *            - An instance of the current map view
     */
    private void initLocation(final MapView mapView) {
	this.mlocManager = (LocationManager) this
		.getSystemService(Context.LOCATION_SERVICE);

	this.mlocListener = new LocationListener() {
	    @Override
	    public void onLocationChanged(final Location location) {

		Log.d(ScreenModeMap.APP_TAG,
			"Location update:  " + location.toString());
		Log.d(ScreenModeMap.APP_TAG, "number of layers: "
			+ ScreenModeMap.this.mMapOverlaysList.size());

		ScreenModeMap.this.updatePositions(mapView, location);

		if (ScreenModeMap.mToggleMyLocation) {
		    ScreenModeMap.animateToUserPosition(20);
		}

	    }

	    @Override
	    public void onProviderDisabled(final String provider) {
		Log.d(ScreenModeMap.APP_TAG, "The provider '" + provider
			+ "' has been disabled!");
	    }

	    @Override
	    public void onProviderEnabled(final String provider) {
		Log.d(ScreenModeMap.APP_TAG, "The provider '" + provider
			+ "' has been enabled!");
	    }

	    @Override
	    public void onStatusChanged(final String provider,
		    final int status, final Bundle extras) {
		Log.d(ScreenModeMap.APP_TAG, "The provider '" + provider
			+ "' new status is: " + status);
	    }
	};

	/**
	 * We declare a new criteria on the provider accuracy and let the system
	 * choose for us the best provider
	 */
	final Criteria criteria = new Criteria();
	criteria.setAccuracy(Criteria.ACCURACY_FINE);
	final String provider = this.mlocManager.getBestProvider(criteria,
		false);
	final Location location = this.mlocManager
		.getLastKnownLocation(provider);

	this.updatePositions(mapView, location);
	this.setGeoPostionsOnMapView(mapView);

	if (location != null) {
	    this.mlocManager.requestLocationUpdates(
		    LocationManager.NETWORK_PROVIDER,
		    this.mGpsUpdateTimeInterval, this.mGpsUpdateMoveInterval,
		    this.mlocListener);
	} else {
	    this.setWaitingMessage(true);
	    this.mlocManager.requestLocationUpdates(
		    LocationManager.GPS_PROVIDER, this.mGpsUpdateTimeInterval,
		    this.mGpsUpdateMoveInterval, this.mlocListener);
	}

    }

    private void showAccuracyArea() {
	if (this.mLocation != null) {
	    final GeoPointProxy point = new GeoPointProxy(
		    (int) (this.mLatitude * 1E6), (int) (this.mLongitude * 1E6));
	    ScreenModeMap.mAccuracyAreaOverlay
		    .setResources(this.getResources());
	    ScreenModeMap.mAccuracyAreaOverlay.setTheGpsAccuracy(this.mLocation
		    .getAccuracy());
	    ScreenModeMap.mAccuracyAreaOverlay.setPointToDraw(point);
	    this.mMapOverlaysList.add(ScreenModeMap.mAccuracyAreaOverlay);
	}
    }

    /**
     * This static method draws a direction from the current user location to a
     * given location. Please note that this method uses the last "cached"
     * direction information that where provided by {@link
     * ScreenModeMap.showDirectionToGeoPoint()}. This method was designed to
     * optimize the direction retrieving process.
     * 
     * @param lastSelectedGeoPoint
     *            - A given location (as a GeoPoint) to where we need to draw a
     *            direction.
     * 
     * @see {@link ScreenModeMap.showDirectionToGeoPoint()}
     */
    private void showCachedDirectionToGeoPoint(
	    final GeoPointProxy lastSelectedGeoPoint) {

	final ScreenModeMap screenModeMap = ScreenModeMap.getInstance();
	try {
	    final List<Overlay> overlays = ScreenModeMap.mMapView.getOverlays();
	    overlays.add(screenModeMap.mCachedDirections);
	    overlays.add(screenModeMap.mCachedPlacemarkers);
	} catch (final Exception e) {
	    this.showToast("Sorry! Your location is not available yet. We are unable to get your direction.");
	}
    }

    private void showCompass() {

	if (ScreenModePrefs.getCompass(this.getApplicationContext())) {
	    this.mMapOverlaysList = ScreenModeMap.mMapView.getOverlays();
	    this.mMapOverlaysList.add(this.mCompass);
	    // this.myLocationOverlay = new MyMyLocationOverlay(this,
	    // ScreenModeMap.mapView);
	    // this.mapOverlaysList.add(this.myLocationOverlay);
	}
    }

    private void updatePositions(final MapView mapView, final Location location) {
	if (location != null) {
	    this.mLocation = location;
	    this.mLatitude = location.getLatitude();
	    this.mLongitude = location.getLongitude();
	    this.setGeoPostionsOnMapView(mapView);
	}
    }
}
