package com.istic.mmm.arcampus.modes;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.istic.mmm.arcampus.R;
import com.istic.mmm.arcampus.shared.PI.GeoPointProxy;
import com.istic.mmm.arcampus.shared.PI.PointsOfInterest;
import com.istic.mmm.arcampus.shared.PI.PointsOfInterestFactory;

import java.util.HashMap;

public class ScreenModePI extends ListActivity {

    protected static final String APP_TAG = "ScreenModePI";
    private static ScreenModePI instance = null;

    public static ScreenModePI getInstance() {
	if (ScreenModePI.instance == null) {
	    ScreenModePI.instance = new ScreenModePI();
	}
	return ScreenModePI.instance;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	final HashMap<String, GeoPointProxy> pointsOfInterest = (new PointsOfInterestFactory())
		.createPointsOfInterest().getPointsOfInterest(this);

	final String[] pi_shortname = PointsOfInterest
		.hashMapToArray(pointsOfInterest);
	final String[] pi_names = new String[pi_shortname.length];
	for (int i = 0; i < pi_shortname.length; i++) {
	    pi_names[i] = pointsOfInterest.get(pi_shortname[i]).getName();
	}

	this.setListAdapter(new ArrayAdapter<Object>(this,
		R.layout.mode_pi_list_item, pi_names));

	final ListView lv = this.getListView();
	lv.setTextFilterEnabled(true);

	lv.setOnItemClickListener(new OnItemClickListener() {
	    @Override
	    public void onItemClick(final AdapterView<?> parent,
		    final View view, final int position, final long id) {

		ScreenModePI.this.hideSoftKeyboard();

		final String pi_name = (String) ((TextView) view).getText();
		final Intent intent = new Intent(ScreenModePI.this,
			ScreenModeMap.class);
		final Bundle b = new Bundle();
		b.putString("selected_pi", pi_name);
		intent.putExtras(b);

		Log.d(ScreenModePI.APP_TAG, "Strating map mode with: '"
			+ pi_name + "'");

		// http://developer.android.com/reference/android/content/Intent.html#setFlags(int)
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		ScreenModePI.this.startActivity(intent);

	    }
	});
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
	super.onCreateOptionsMenu(menu);
	final MenuInflater inflater = this.getMenuInflater();
	inflater.inflate(R.layout.mode_pi_menu, menu);
	return true;
    }

    @Override
    public boolean onMenuItemSelected(final int featureId, final MenuItem item) {
	this.showSoftKeyboard();
	return super.onMenuItemSelected(featureId, item);
    }

    @Override
    protected void onPause() {
	super.onPause();
	this.finish();
    }

    private void hideSoftKeyboard() {
	final InputMethodManager imm = (InputMethodManager) this
		.getSystemService(Context.INPUT_METHOD_SERVICE);
	imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    private void showSoftKeyboard() {
	final InputMethodManager imm = (InputMethodManager) this
		.getSystemService(Context.INPUT_METHOD_SERVICE);
	imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

}
