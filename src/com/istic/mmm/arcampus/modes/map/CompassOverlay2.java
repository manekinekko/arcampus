package com.istic.mmm.arcampus.modes.map;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class CompassOverlay2 extends Overlay {
    Paint mPaint = new Paint();
    private final Path mPath = new Path();
    private float azimut = 0;
    float[] mGravity;
    float[] mGeomagnetic;

    private final SensorEventListener mListener = new SensorEventListener() {
	@Override
	public void onAccuracyChanged(final Sensor sensor, final int accuracy) {
	}

	@Override
	public void onSensorChanged(final SensorEvent event) {
	    if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
		CompassOverlay2.this.mGravity = event.values;
	    }
	    if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
		CompassOverlay2.this.mGeomagnetic = event.values;
	    }
	    if ((CompassOverlay2.this.mGravity != null)
		    && (CompassOverlay2.this.mGeomagnetic != null)) {
		final float R[] = new float[9];
		final float I[] = new float[9];
		final boolean success = SensorManager.getRotationMatrix(R, I,
			CompassOverlay2.this.mGravity,
			CompassOverlay2.this.mGeomagnetic);
		if (success) {
		    final float orientation[] = new float[3];
		    SensorManager.getOrientation(R, orientation);
		    CompassOverlay2.this.azimut = orientation[0];
		}
	    }
	}
    };
    private final Context mContext;

    public CompassOverlay2(final Context context) {
	this.mContext = context;

	this.mPaint.setAntiAlias(true);
	this.mPaint.setColor(Color.RED);
	this.mPaint.setStyle(Paint.Style.FILL);

	// Construct a wedge-shaped path
	this.mPath.moveTo(0, -50);
	this.mPath.lineTo(-20, 60);
	this.mPath.lineTo(0, 50);
	this.mPath.lineTo(20, 60);
	this.mPath.close();
    }

    @Override
    public boolean draw(final Canvas canvas, final MapView mapView,
	    final boolean shadow, final long when) {
	if (shadow == false) {

	    canvas.save();

	    final int w = canvas.getWidth();
	    final int h = canvas.getHeight();
	    final int cx = w - 70;
	    final int cy = h / 8;
	    canvas.translate(cx, cy);

	    // Rotate the canvas with the azimut
	    if (this.azimut != 0) {
		canvas.rotate((float) ((-this.azimut * 360) / (2 * Math.PI)));
	    }
	    canvas.drawPath(this.mPath, this.mPaint);
	    // canvas.drawText("N", cx + 0.2f, cy, this.mPaint);
	    // canvas.drawText("S", cx - 10, cy + 1, this.mPaint);

	    canvas.restore();
	}
	return true;
    };

    public float getAzimut() {
	return this.azimut;
    }

    public SensorEventListener getListener() {
	return this.mListener;
    }

    public void setAzimut(final float azimut) {
	this.azimut = azimut;
    }
}
