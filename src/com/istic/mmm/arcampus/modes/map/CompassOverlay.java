package com.istic.mmm.arcampus.modes.map;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class CompassOverlay extends Overlay {
	private final Paint mPaint = new Paint();
	private final Path mPath = new Path();
	private final Paint mTextPaint = new Paint();
	private boolean mAnimate;
	private float[] mValues;

	private final SensorEventListener mListener = new SensorEventListener() {
		@Override
		public void onAccuracyChanged(final Sensor sensor, final int accuracy) {
		}

		@Override
		public void onSensorChanged(final SensorEvent event) {
			CompassOverlay.this.mValues = event.values;
			if (CompassOverlay.this != null) {
			}
		}
	};

	public CompassOverlay() {

		// Construct a wedge-shaped path
		this.mPath.moveTo(0, -50);
		this.mPath.lineTo(-20, 60);
		this.mPath.lineTo(0, 50);
		this.mPath.lineTo(20, 60);
		this.mPath.close();
	}

	@Override
	public boolean draw(final Canvas canvas, final MapView mapView,
			final boolean shadow, final long when) {

		canvas.save(); // important before modifying the state !!
		super.draw(canvas, mapView, shadow, when);

		final Paint paint = this.mPaint;
		paint.setAntiAlias(true);
		paint.setColor(Color.RED);
		paint.setStyle(Paint.Style.FILL);

		final int w = canvas.getWidth();
		final int h = canvas.getHeight();
		final int cx = w - 70;
		final int cy = h / 8;

		canvas.translate(cx, cy);
		if (CompassOverlay.this.mValues != null) {
			canvas.rotate(-CompassOverlay.this.mValues[0]);
		}
		canvas.drawPath(this.mPath, this.mPaint);

		canvas.restore(); // important (after saving the state) !!
		return true;
	}

	public SensorEventListener getListener() {
		return this.mListener;
	}
}
