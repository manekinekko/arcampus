package com.istic.mmm.arcampus.modes.map;


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

import java.util.List;

public class DirectionPathOverlay extends Overlay {
    private final List<GeoPoint> mGeoPointList;

    public DirectionPathOverlay(final List<GeoPoint> geoPoints) {
        this.mGeoPointList = geoPoints;
    }

    @Override
    public void draw(final Canvas canvas, final MapView mapView, final boolean shadow) {
        super.draw(canvas, mapView, shadow);
    }

    @Override
    public boolean draw(final Canvas canvas, final MapView mapView, final boolean shadow,
            final long when) {
        if (shadow == false) {
            final Projection projection = mapView.getProjection();

            // geopoint
            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(Color.BLUE);
            paint.setAlpha(100);
            paint.setStrokeWidth(6);
            Point point1, point2;
            GeoPoint geo1, geo2;
            for (int i = 1; i < this.mGeoPointList.size(); i++) {
                geo1 = this.mGeoPointList.get(i - 1);
                geo2 = this.mGeoPointList.get(i);
                point1 = new Point();
                point2 = new Point();
                projection.toPixels(geo1, point1);
                projection.toPixels(geo2, point2);
                canvas.drawLine(point1.x, point1.y, point2.x, point2.y, paint);
            }

        }
        return super.draw(canvas, mapView, shadow, when);
    }
}
