package com.istic.mmm.arcampus.modes.map;

import android.content.Context;
import android.util.Log;

import com.google.android.maps.Overlay;
import com.istic.mmm.arcampus.modes.ScreenModeMap;
import com.istic.mmm.arcampus.shared.utils.driving.DrivingDirections.IDirectionsListener;
import com.istic.mmm.arcampus.shared.utils.driving.DrivingDirections.Mode;
import com.istic.mmm.arcampus.shared.utils.driving.Route;

import java.util.List;

public class DrivingDirectionCallback implements IDirectionsListener {

    private final Context mContext;

    public DrivingDirectionCallback(final Context ctx) {
	this.mContext = ctx;
    }

    @Override
    public void onDirectionsAvailable(final Route route, final Mode mode) {
	if (route == null) {
	    return;
	}

	final List<Overlay> overlays = ((ScreenModeMap) this.mContext)
		.getMapView().getOverlays();

	final DirectionPathOverlay directionPathOverlay = new DirectionPathOverlay(
		route.getGeoPoints());

	((ScreenModeMap) this.mContext)
		.setCachedDirections(directionPathOverlay);

	final DirectionPlacemarkOverlay directionPlacemarkOverlay = new DirectionPlacemarkOverlay(
		route.getPlacemarks());

	((ScreenModeMap) this.mContext)
		.setCachedPlacemarks(directionPlacemarkOverlay);

	overlays.add(directionPathOverlay);
	overlays.add(directionPlacemarkOverlay);
	ScreenModeMap.animateToUserPosition(18);
	ScreenModeMap.setDirectionPath(true);

	((ScreenModeMap) this.mContext).setWaitingMessage(false);

	// final List<Placemark> pm = route.getPlacemarks();
	// for (final Placemark placemark : pm) {
	// final Placemark p = placemark;
	// Log.d("", p.getInstructions() + "\n" + p.getDistance() + "\n"
	// + p.getLocation().toString());
	// }

    }

    @Override
    public void onDirectionsNotAvailable() {
	Log.d("DrivingDirectionCallback",
		"We could not find your direction. Please try later.");
	ScreenModeMap.setDirectionPath(false);
    }

}
