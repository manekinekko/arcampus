package com.istic.mmm.arcampus.modes.map;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class AccuracyAreaOverlay extends Overlay {

	private Paint mPaintBorder, mPaintFill;
	private GeoPoint pointToDraw;
	private Resources resources;
	private float theGpsAccuracy;
	private boolean canDraw;
	private Paint mPaintText;

	public void clear() {
		this.resources = null;
	}

	@Override
	public boolean draw(final Canvas canvas, final MapView mapView,
			final boolean shadow, final long when) {
		super.draw(canvas, mapView, shadow);

		if (this.canDraw) {

			// convert point to pixels
			final Point screenPts = new Point();
			mapView.getProjection().toPixels(this.pointToDraw, screenPts);

			// add marker
			this.mPaintBorder = new Paint();
			this.mPaintBorder.setStyle(Paint.Style.STROKE);
			this.mPaintBorder.setAntiAlias(true);
			this.mPaintBorder.setColor(0xEE4D2EFF);
			this.mPaintFill = new Paint();
			this.mPaintFill.setStyle(Paint.Style.FILL);
			this.mPaintFill.setColor(0x154D2EFF);

			final int radius = (int) mapView.getProjection()
					.metersToEquatorPixels(this.theGpsAccuracy);
			canvas.drawCircle(screenPts.x, screenPts.y, radius,
					this.mPaintBorder);
			canvas.drawCircle(screenPts.x, screenPts.y, radius, this.mPaintFill);

			// add text
			this.mPaintText = new Paint();
			this.mPaintText.setStyle(Paint.Style.STROKE);
			this.mPaintText.setAntiAlias(true);
			this.mPaintText.setColor(0xFFFFFFFF);
			this.mPaintText.setTextSize(20f);
			canvas.drawText(this.theGpsAccuracy + "m", screenPts.x + radius,
					screenPts.y, this.mPaintText);

		}
		return true;
	}

	public GeoPoint getPointToDraw() {
		return this.pointToDraw;
	}

	public Resources getResources() {
		return this.resources;
	}

	public float getTheGpsAccuracy() {
		return this.theGpsAccuracy;
	}

	public boolean isDrawn() {
		return this.canDraw;
	}

	public void setDrawState(final boolean state) {
		this.canDraw = state;
	}

	public void setPointToDraw(final GeoPoint point) {
		this.pointToDraw = point;
	}

	public void setResources(final Resources resources) {
		this.resources = resources;
	}

	public void setTheGpsAccuracy(final float theGpsAccuracy) {
		this.theGpsAccuracy = theGpsAccuracy;
	}

	public void toggleDraw() {
		if (!this.isDrawn()) {
			this.setDrawState(true);
		} else {
			this.setDrawState(false);
			this.clear();
		}
	}

}
