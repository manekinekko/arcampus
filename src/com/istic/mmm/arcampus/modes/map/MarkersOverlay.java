package com.istic.mmm.arcampus.modes.map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.drawable.Drawable;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;
import com.istic.mmm.arcampus.ApplicationConfig;
import com.istic.mmm.arcampus.modes.ScreenModeMap;
import com.istic.mmm.arcampus.shared.PI.GeoPointProxy;

import java.util.ArrayList;

public class MarkersOverlay extends ItemizedOverlay<OverlayItem> implements
	OnClickListener {

    private final Context mContext;
    private final ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
    private AlertDialog mDialog;
    private static GeoPointProxy mLastSelectedGeoPoint;

    public MarkersOverlay(final Drawable defaultMarker, final Context context) {
	super(ItemizedOverlay.boundCenterBottom(defaultMarker));
	this.mContext = context;
    }

    public static GeoPointProxy getLastSelectedGeoPoint() {
	return MarkersOverlay.mLastSelectedGeoPoint;
    }

    public void addOverlay(final OverlayItem overlay) {
	this.mOverlays.add(overlay);
	this.populate();
    }

    @Override
    public void onClick(final DialogInterface dialog, final int which) {
	switch (which) {
	case -1:
	    break;
	case -3:
	    break;
	default:
	    // the cancel button
	}
    }

    @Override
    public int size() {
	return this.mOverlays.size();
    }

    protected void clearPreviousDirectionPath() {
	final ScreenModeMap smm = ((ScreenModeMap) this.mContext);
	// this will clear the map overlays !!
	ScreenModeMap.setDirectionPath(false);
	smm.setGeoPostionsOnMapView(smm.getMapView());
    }

    @Override
    protected OverlayItem createItem(final int i) {
	return this.mOverlays.get(i);
    }

    @Override
    protected boolean onTap(final int index) {
	final OverlayItem item = this.mOverlays.get(index);
	MarkersOverlay.mLastSelectedGeoPoint = (GeoPointProxy) item.getPoint();

	final AlertDialog.Builder builder = new AlertDialog.Builder(
		this.mContext);
	builder.setTitle(item.getTitle())
		.setMessage(item.getSnippet())
		.setIcon(
			ApplicationConfig
				.getPIDrawable(MarkersOverlay.mLastSelectedGeoPoint
					.getIcon()));

	if (MarkersOverlay.mLastSelectedGeoPoint.getName().equals("")) {

	    if (((ScreenModeMap) this.mContext).isDirectionPathOn()) {
		builder.setNeutralButton("Clear directions",
			new OnClickListener() {
			    @Override
			    public void onClick(final DialogInterface dialog,
				    final int which) {
				MarkersOverlay.this
					.clearPreviousDirectionPath();
			    }
			});
	    }

	} else {

	    builder.setNeutralButton("Show directions", new OnClickListener() {
		@Override
		public void onClick(final DialogInterface dialog,
			final int which) {

		    MarkersOverlay.this.clearPreviousDirectionPath();

		    final Context ctx = MarkersOverlay.this.mContext;
		    ((ScreenModeMap) ctx).setWaitingMessage(true);
		    ((ScreenModeMap) ctx).showDirectionToGeoPoint(
			    MarkersOverlay.mLastSelectedGeoPoint, true);
		}
	    });

	}

	builder.setNegativeButton("Close", this);
	this.mDialog = builder.show();

	return true;
    }
}
