package com.istic.mmm.arcampus.modes.map;


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;
import com.istic.mmm.arcampus.shared.utils.driving.Placemark;

import java.util.List;

public class DirectionPlacemarkOverlay extends Overlay {

    private final List<Placemark> mPlacemarks;

    public DirectionPlacemarkOverlay(final List<Placemark> placemarks) {
        this.mPlacemarks = placemarks;
    }

    @Override
    public void draw(final Canvas canvas, final MapView mapView, final boolean shadow) {
        super.draw(canvas, mapView, shadow);
    }

    @Override
    public boolean draw(final Canvas canvas, final MapView mapView, final boolean shadow,
            final long when) {
        if (shadow == false) {
            final Projection projection = mapView.getProjection();
            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setStrokeWidth(1);

            Point point;
            Placemark pm;
            int radius = 10;
            for (int i = 0; i < this.mPlacemarks.size(); i++) {

                // first
                if (i == 0) {
                    paint.setColor(Color.GREEN);
                    radius = 10;
                }
                // last
                else if (i == this.mPlacemarks.size() - 1) {
                    paint.setColor(Color.RED);
                    radius = 10;
                }
                // others
                else {
                    paint.setColor(Color.BLUE);
                    radius = 5;
                }
                pm = this.mPlacemarks.get(i);
                point = new Point();
                projection.toPixels(pm.getLocation(), point);
                canvas.drawCircle(point.x, point.y, radius, paint);
            }

        }
        return super.draw(canvas, mapView, shadow, when);
    }

}
