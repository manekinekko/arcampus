package com.istic.mmm.arcampus.modes;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;

import com.istic.mmm.arcampus.ApplicationConfig;
import com.istic.mmm.arcampus.R;
import com.istic.mmm.arcampus.shared.utils.JsonPullParser;
import com.istic.mmm.arcampus.shared.utils.JsonUpdater;

import java.io.File;

public class ScreenModePrefs extends PreferenceActivity implements
	OnSharedPreferenceChangeListener {

    // Static method to return the preference for whether to display compass
    public static boolean getCompass(final Context context) {
	return PreferenceManager.getDefaultSharedPreferences(context)
		.getBoolean("compass", true);
    }

    // Static method to return the preference for the GPS precision setting
    public static String getGPSPref(final Context context) {
	return PreferenceManager.getDefaultSharedPreferences(context)
		.getString("gpsPref", "1");
    }

    // Inherited abstract method so it must be implemented
    @Override
    public void onSharedPreferenceChanged(
	    final SharedPreferences sharedPreferences, final String key) {
	Log.i("Preferences", "Preferences changed, key=" + key);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	this.addPreferencesFromResource(R.xml.prefs);

	// Register a change listener

	final Context context = this.getApplicationContext();
	final SharedPreferences prefs = PreferenceManager
		.getDefaultSharedPreferences(context);
	prefs.registerOnSharedPreferenceChangeListener(this);

	final Preference customPref = this.findPreference("update");

	final String piPath = Environment.getExternalStorageDirectory()
		+ File.separator + this.getString(R.string.app_name)
		+ File.separator + ApplicationConfig.PI_FILENAME;

	String title = customPref.getTitle().toString();
	title += " (actual version: " + JsonPullParser.getVersion(piPath) + ")";
	customPref.setTitle(title);

	customPref
		.setOnPreferenceClickListener(new OnPreferenceClickListener() {

		    @Override
		    public boolean onPreferenceClick(final Preference preference) {

			final Intent intent = new Intent(context,
				JsonUpdater.class);
			intent.putExtra("caller", "manual");
			context.startService(intent);
			return true;
		    }

		});

    }
}
