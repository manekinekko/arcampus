package com.istic.mmm.arcampus.modes.ar;

public interface EventMarkerManager {
	void markerTouched(ARMarker marker);
	void markerTouchedSecond(ARMarker marker);
	void menuTouched(ARMarker marker);
	void cameraTouched();
}
