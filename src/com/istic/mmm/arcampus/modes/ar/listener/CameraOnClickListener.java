package com.istic.mmm.arcampus.modes.ar.listener;

import com.istic.mmm.arcampus.modes.ar.EventMarkerManager;

import android.view.View;
import android.view.View.OnClickListener;

public class CameraOnClickListener implements OnClickListener {

	private EventMarkerManager manager;
	
	public CameraOnClickListener(EventMarkerManager manager) {
		
		this.manager = manager;
	}
	
	
	@Override
	public void onClick(View v) {
		
		manager.cameraTouched();
	}

}
