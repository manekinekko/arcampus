package com.istic.mmm.arcampus.modes.ar.listener;

import com.istic.mmm.arcampus.modes.ar.ARMarker;
import com.istic.mmm.arcampus.modes.ar.EventMarkerManager;

import android.view.View;
import android.view.View.OnClickListener;

public class MarkerOnClickListener implements OnClickListener {

	private ARMarker marker;
	private EventMarkerManager manager;
	
	public MarkerOnClickListener(EventMarkerManager manager, ARMarker marker) {
		
		this.manager = manager;
		this.marker = marker;
	}
	
	
	@Override
	public void onClick(View v) {
		
		manager.markerTouched(marker);
	}

}
