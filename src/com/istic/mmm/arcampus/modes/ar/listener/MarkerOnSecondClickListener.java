package com.istic.mmm.arcampus.modes.ar.listener;

import com.istic.mmm.arcampus.modes.ar.ARMarker;
import com.istic.mmm.arcampus.modes.ar.EventMarkerManager;

import android.view.View;
import android.view.View.OnClickListener;

public class MarkerOnSecondClickListener implements OnClickListener {

	private ARMarker marker;
	private EventMarkerManager manager;
	
	public MarkerOnSecondClickListener(EventMarkerManager manager, ARMarker marker) {
		
		this.manager = manager;
		this.marker = marker;
	}
	
	
	@Override
	public void onClick(View v) {
		
		manager.markerTouchedSecond(marker);
	}

}