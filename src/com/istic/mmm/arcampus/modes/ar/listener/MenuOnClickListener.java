package com.istic.mmm.arcampus.modes.ar.listener;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.istic.mmm.arcampus.modes.ar.ARMarker;
import com.istic.mmm.arcampus.modes.ar.EventMarkerManager;

public class MenuOnClickListener implements OnClickListener {

	private ARMarker marker;
	private EventMarkerManager manager;
	
	public MenuOnClickListener(EventMarkerManager manager, ARMarker marker) {
		
		this.manager = manager;
		this.marker = marker;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		 
		manager.menuTouched(marker);
	}
}