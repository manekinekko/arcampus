package com.istic.mmm.arcampus.modes.ar;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/* This class manage the camera input for AR mode
 */
public class CameraView extends SurfaceView implements SurfaceHolder.Callback {
	
	private Camera 			camera;
	private SurfaceHolder 	holder;
	private Parameters 		parameters;
	
	public CameraView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		holder = getHolder();
	    holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		holder.addCallback(this);
		
		setKeepScreenOn(true); // disable screen economizer
	}
	
	public CameraView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,	int height) {
		
		try {
			parameters = camera.getParameters();
			parameters.setPreviewSize(800, 480);
			camera.setParameters(parameters);
			camera.setPreviewDisplay(holder);
			camera.startPreview();
		} catch (Exception e) {
			
		}
	}

	/*
	 * init Camera
	 */
	@Override
	public void surfaceCreated(SurfaceHolder _holder) {
		camera = Camera.open();
		try {
			camera.setPreviewDisplay(_holder);
		} catch (Exception e) {
		}
		
	}

	/*
	 * destroy camera resource before close application
	 */
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		camera.stopPreview();
		camera.release();		
	}

}
