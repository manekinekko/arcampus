package com.istic.mmm.arcampus.modes.ar;

import android.location.Location;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

import com.google.android.maps.GeoPoint;

public class MathUtil {
	
	public final static float getDistance(GeoPoint p, Location _me) {
		Location location = new Location("LOCATION_SERVICE");
		try {
			location.setLatitude(p.getLatitudeE6()/1E6);
			location.setLongitude(p.getLongitudeE6()/1E6);
			float distance = _me.distanceTo(location);
			return distance;
		} catch (Exception e) {
			return -1;
		}
	}
	
	public final static float getSpotAngle(GeoPoint p, Location _me) {
		Location location = new Location("LOCATION_SERVICE");
		try {
			location.setLatitude(p.getLatitudeE6()/1E6);
			location.setLongitude(p.getLongitudeE6()/1E6);
		} catch (Exception e) {
		}
		return _me.bearingTo(location);
	}
	
	public final static double angleDirection(double spotAngle, double direction) {
		double angle;
		if (spotAngle > 0) {
			if (direction < spotAngle - 180)
				angle = 360 - spotAngle + direction;
			else
				angle = direction - spotAngle;
		} else {
			if (direction > spotAngle + 180)
				angle = direction - spotAngle - 360;
			else
				angle = direction - spotAngle;
		}
		return (angle);
	}

	public final static void moveSpot(View tv, GeoPoint p, float azimut, Location me, int screenWidth, float roll, int screenHeight, float pitch)
	{
		int angle = (int) (angleDirection(getSpotAngle(p,me), azimut));

		LayoutParams lp = new FrameLayout.LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT); 
		int marginTop;
		if(pitch<screenHeight/2)
			marginTop = (int)((roll-90)/90*screenHeight);
		else
			marginTop = -(int)((roll-90)/90*screenHeight);
		
		lp.setMargins(-angle*screenWidth/90,marginTop, 0, 0);
		lp.gravity=Gravity.CENTER;
		tv.setLayoutParams(lp);
	}
	
	public final static void moveTrackedSpot(View tv, GeoPoint p, float azimut, Location me, int screenWidth, float roll, int screenHeight, float pitch)
	{
		int angle = (int) (angleDirection(getSpotAngle(p,me), azimut));

		LayoutParams lp = new FrameLayout.LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT); 
		int marginTop;
		if(pitch<screenHeight/2)
			marginTop = (int)((roll-90)/90*screenHeight);
		else
			marginTop = -(int)((roll-90)/90*screenHeight);
		
		int leftPosition = -angle*screenWidth/90;
		
		if(leftPosition<(-screenWidth+tv.getWidth())/2)
			lp.setMargins((-screenWidth+tv.getWidth())/2, marginTop, 0, 0);
		else if(leftPosition>(screenWidth-tv.getWidth())/2)
			lp.setMargins((screenWidth-tv.getWidth())/2, marginTop, 0, 0);
		else
			lp.setMargins(leftPosition, marginTop, 0, 0);
		
		lp.gravity=Gravity.CENTER;
		tv.setLayoutParams(lp);
	}
}
