package com.istic.mmm.arcampus.modes.ar;

import com.istic.mmm.arcampus.shared.PI.GeoPointProxy;

import android.view.View;
import android.widget.TextView;

/* This class contains all information about a PI for AR mode
 * It's probably a temporary class because we will merge this kind of
 * data structure with the mode map...
 */
public class ARMarker implements Comparable<ARMarker> {
	
	private View view, selectedView;
	private TextView locationTextView, locationTextViewSelected;
	private GeoPointProxy geoPoint;
	private boolean refreshed, selected;
	private float distanceToUser;
	
	public ARMarker(View view, TextView locationTextView, View selectedView, TextView locationTextViewSelected) {
		
		this.view = view;
		this.selectedView = selectedView;
		this.locationTextView = locationTextView;
		this.locationTextViewSelected = locationTextViewSelected;
		selected = false;
		refreshed = false;
		setDistanceToUser(-1);
	}
	
	public String toString() {
		return "getTitle : "+getTitle()+
		"\n"+"getDistanceToUser : "+getDistanceToUser()+
		"\n"+"isSelected : "+isSelected()+
		"\n"+"getView : "+getView().toString()+
		"\n"+"getSelectedView : "+getSelectedView().toString()+
		"\n"+"getCurrentView : "+getCurrentView().toString();
	}
	
	public float getDistanceToUser() {
		return distanceToUser;
	}

	public void setDistanceToUser(float distanceToUser) {
		
		this.distanceToUser = distanceToUser;
		if(this.distanceToUser>0){
			locationTextView.setText(((int)this.distanceToUser)+"m");
			locationTextViewSelected.setText(((int)this.distanceToUser)+"m");
		} else {
			locationTextView.setText("ND");
			locationTextViewSelected.setText("ND");
		}
	}

	public boolean isRefreshed() {
		
		return refreshed;
	}
	
	public void setRefreshed() {
		
		this.refreshed = true;
	}
	
	public boolean isSelected() {

		return selected;
	}
	
	public void setSelected(boolean selected) {

		if(this.selected != selected) 
			refreshed = false;
		this.selected = selected;
	}
	
	public View getView() {
		return view;
	}
	
	public View getSelectedView() {

		return selectedView;
	}
	
	public View getCurrentView() {
		if(selected)
			return selectedView;
		else
			return view;
	}
	
	public void setView(View view) {
		this.view = view;
	}
	public GeoPointProxy getGeoPoint() {
		return geoPoint;
	}
	public void setGeoPoint(GeoPointProxy geoPoint) {
		if(this.geoPoint != geoPoint) 
			refreshed = false;
		this.geoPoint = geoPoint;
	}

	public String getTitle() {
		return geoPoint.getName();
	}

	public String getSnippet() {
		return geoPoint.getComment();
	}

	@Override
	public int compareTo(ARMarker e2) {
		
		if(getDistanceToUser() < e2.getDistanceToUser() || isSelected()) {
			return 1;
		} else if(getDistanceToUser() == e2.getDistanceToUser()) {
			return 0;
		}
		else {
			return -1;
		}
	}
}
