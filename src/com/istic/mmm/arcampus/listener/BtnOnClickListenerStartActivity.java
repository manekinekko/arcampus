package com.istic.mmm.arcampus.listener;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

public class BtnOnClickListenerStartActivity implements OnClickListener {

	private final Class<?> c_;
	private final Context ctx_;

	public BtnOnClickListenerStartActivity(final Context ctx, final Class<?> c) {
		this.ctx_ = ctx;
		this.c_ = c;
	}

	@Override
	public void onClick(final View v) {
		final Intent i = new Intent(this.ctx_, this.c_);
		this.ctx_.startActivity(i);
		// ((Activity) this.ctx_).finish();
	}

}
