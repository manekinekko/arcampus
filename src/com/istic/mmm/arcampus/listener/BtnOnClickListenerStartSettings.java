package com.istic.mmm.arcampus.listener;


import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;


public class BtnOnClickListenerStartSettings implements OnClickListener {

    private final Context ctx_;
    private final String setting_;

    public BtnOnClickListenerStartSettings(Context ctx, String setting) {
        this.ctx_ = ctx;
        this.setting_ = setting;
    }

    @Override
    public void onClick(View v) {
        this.ctx_.startActivity(new Intent(this.setting_));
    }

}
