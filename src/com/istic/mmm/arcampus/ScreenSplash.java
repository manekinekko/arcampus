package com.istic.mmm.arcampus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;

public class ScreenSplash extends Activity {

    protected int _splashTime = 2000;

    private Thread splashTread;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(final Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	this.setContentView(R.layout.screen_splash);
	ApplicationConfig.prepareAppDirectoryOnSDCard(this);
	final Activity activity = this;

	this.splashTread = new Thread() {
	    @Override
	    public void run() {
		try {
		    synchronized (this) {
			this.wait(ScreenSplash.this._splashTime);
		    }

		} catch (final InterruptedException e) {
		} finally {
		    ScreenSplash.this.finish();

		    final Intent i = new Intent(ScreenSplash.this,
			    ScreenHome.class);
		    ScreenSplash.this.startActivity(i);

		    ApplicationConfig
			    .runAlarm(
				    ScreenSplash.this.getApplicationContext(),
				    activity);

		    this.interrupt();
		}
	    }
	};

	this.splashTread.start();
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
	if (event.getAction() == MotionEvent.ACTION_DOWN) {
	    synchronized (this.splashTread) {
		this.splashTread.notifyAll();
	    }
	}
	return true;
    }

    @Override
    protected void onPause() {
	super.onPause();
	ScreenSplash.this.finish();
    }

}
